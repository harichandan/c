#include<stdio.h>

int reverse(int n) {
  int rev=0;
  while(n) {
    rev=rev*10+(n%10);
    n/=10;
  }
  return rev;
}

int isPalindrome(int n) {
  if(n==reverse(n)) 
    return 0;
  return 1;
}

int main() {
  int T, L, R;
  int sum=0, i, k;
  scanf(" %d", &T);
  for(k=0; k<T; k++) {
    scanf(" %d %d", &L, &R);
    for(i=L; i<=R; i++) {
      if(!isPalindrome(i)) {
        sum+=i;
      }
    }
    printf("%d\n", sum); 
    sum=0;
  }
  return 0;
}
