#include <stdio.h>

long fact(int n) {
  long factorial=1;
  int i;
  if(n==0||n==1)
    return factorial;
  for(i=n;i>=1; i--) {
    factorial*=i;
  }
  return factorial;
}

int main() {
  int i, n, T;
  scanf(" %d", &T);
  for(i=0; i<T; i++) {
    scanf(" %d", &n);
    printf("%ld\n", fact(n));
  }
  return 0;
}
