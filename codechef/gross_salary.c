#include <stdio.h>

int main() {
  int salary, i, T;
  float gross=0;
  scanf(" %d", &T);
  for(i=0; i<T; i++) {
    scanf(" %d", &salary);
    if(salary<1500)
      printf("%g\n", salary+0.1*salary+0.9*salary);
    else
      printf("%g\n", salary+500+0.98*salary);
  }
  return 0;
}
