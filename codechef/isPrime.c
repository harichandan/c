#include<stdio.h>

int isPrime(int n) {
  if(n<0||n==0||n==1)
    return 0;
  if(n==2)
    return 1;
  for(int i=3; i<n; i+=2) {
    if(n%i==0)
      return 0;
  }
  return 1;
}

int main() {
  int i, T, N;
  scanf(" %d", &T);
  for(i=0; i<T; i++) {
    scanf(" %d", &N);
    if(isPrime(N))
      printf("yes\n");
    else
      printf("no\n");
  }
  return 0;
}
