#include <stdio.h>

int sum_dig(int n) {
  int sum=0;
  while(n) {
    sum+=n%10;
    n/=10;
  }
  return sum;
}

int main() {
  int T,N,i;
  scanf(" %d", &T);
  for(i=0; i<T; i++) {
    scanf(" %d", &N);
    printf("%d\n", sum_dig(N));
  }
  return 0;
}
