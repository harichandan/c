#include <stdio.h>

int rev_num(int n) {
  int rev=0;
  while(n) {
    rev=rev*10+n%10;
    n/=10;
  }
  return rev;
}

int main() {
  int T,N,i;
  scanf(" %d", &T);
  for(i=0; i<T; i++) {
    scanf(" %d", &N);
    printf("%d\n", rev_num(N));
  }
  return 0;
}
