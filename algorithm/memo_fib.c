#include <stdio.h>

int a[100];

int fib(int n) {
  if (a[n] != 0)
    return a[n];
  if (n <= 2)
    return n;
  else { 
    int f = fib(n-1) + fib(n-2);
    a[n] = f;
    return f;
  }
}

int main() {
  int n;
  printf ("Enter n: ");
  scanf(" %d", &n);
  printf ("%dth term is: %d\n", n, fib(n));
  return 0;
}
