#include <stdio.h>
#include <string.h>

int hex(char c) {
  char hexstring[] = "0123456789ABCDEF";
  for (int i=0; i<16; i++) {
    if(c == hexstring[i])
      return i;
  }
  return -1;
}
int power(int base, int exponent) {
  int num = base;
  if (exponent == 0)
    return 1;
  else if (exponent == 1)
    return base;
  while (exponent > 1) {
    base = base * num;
    exponent--;
  }
  return base;
}

int hexToDec(char* str, int len) {
  int i, dec=0;
  for (i=len-2; i>=0; i--)
    dec += hex(*(str+i)) * power(16, len-i-2); 
  return dec;
}

int main() {
  char toDec[10];
  printf("Enter a hexadecimal number: ");
  fgets(toDec, sizeof(toDec), stdin);
  printf("The decimal equivalent is: %d\n", hexToDec(toDec, strlen(toDec)));
  return 0;
}
