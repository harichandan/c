// Print reverse of string recursively
#include <stdio.h>

void print_reverse(char *s)
{
    if (*s == '\0')
        return;
    print_reverse(s+1);
    printf("%c", *s);
}

int main()
{
    print_reverse("Harichandan");
    printf("\n");
    return 0;
}
