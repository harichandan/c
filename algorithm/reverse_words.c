#include <stdio.h>
#include <stdlib.h>

void reverse(char *string, int start, int end) {
  char temp;
  int i, j;
  for (i=start, j=end-1; i<(start+end)/2; i++, j--) {
    temp = *(string+i);
    *(string+i) = *(string+j);
    *(string+j) = temp;
  }
}


void reverse_words(char *str, int l) {
  int i, j = 0;

  for (i=0; i<=l; i++) {
    if(*(str+i) == ' ' || *(str+i) == '\0') {
      reverse(str, j, i);
      j = i+1;
    }
  }
}

int main(int argc, char *argv[]) {
  if(argc<2) {
    fprintf(stderr, "Usage: ./exe <string to be reversed>\n");
    return -1;
  }

  char* str = argv[1];
  int l;
  
  for(l=0; *(str+l)!='\0'; l++);

  reverse(str, 0, l);
  reverse_words(str, l);
  printf("String with reversed words: %s\n", str);

  return 0;
}
