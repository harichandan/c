#include <stdio.h>
#include <stdlib.h>

int str_to_num(char *str) {
  int sign = 0, num;
  
  if(*str == '-')
    sign = 1;

  for(int i=sign; *(str+i) != '\0'; i++) {
    if(*(str+i) < '0' || *(str+i) > '9') {
      fprintf(stderr, "Invalid character detected! Exiting!\n");
      exit(-1);
    }
    num = num*10 + (*(str+i)-'0');
  }

  if(sign)
    num = -1*num;

  return num;
}

int main(int argc, char *argv[]) {
  if(argc < 2) {
    fprintf(stderr, "Usage: ./exe <string to be converted>\n");
    return -1;
  }

  printf("The number is: %d\n", str_to_num(argv[1]));

  return 0;
}
