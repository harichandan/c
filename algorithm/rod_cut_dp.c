#include <stdio.h>
#include <stdlib.h>
int max(int a, int b) {
	return (a > b) ? a : b;
}

int bottom_up(int *p, int length) {
	int i, k, *cut = malloc(sizeof(int) * (length+1));
	
	for(i=0; i<=length; i++)
		cut[i] = 0;

	for(i=1; i<=length; i++) {
		int max_val = -1;
		for(k=0; k<i; k++) {
			max_val = max(max_val, p[k] + cut[i-k-1]);
		}
		cut[i] = max_val;
	}

	return cut[length];
}

int top_down(int *p, int length) {
	if (length == 0)
		return 0;

	int max_val = -1;

	for(int i=0; i<length; i++) {
		max_val = max(max_val, p[i] + top_down(p, length-1-i));
	}
	return max_val;
}

int main() {
	int p[] = {1, 5, 8, 9, 10, 17, 17, 20};
	int length = sizeof(p) / sizeof(p[0]);
	printf("Max price of the cut (using bottom up) is: %d\n", bottom_up(p, length));
	printf("Max price of the cut (using top down) is: %d\n", top_down(p, length));
	return 0;
}
