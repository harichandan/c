#include <stdio.h>
#include <stdlib.h>

char *reverse(int num) {
	int l = 0, temp = num, i = 0;
	while (temp) {
		l++;
		temp /= 10;
	}
	char *rev = malloc((l+1)*sizeof(char));
	temp = num;
	while (temp) {
		*(rev+i) = (temp % 10) + '0';
		i++;
		temp /= 10;
	}
	*(rev+i) = '\0';
	return rev;
}

int main() {
	int num;
	printf ("Enter a number : ");
	scanf (" %d", &num);
	char *rev = reverse(num);
	printf("Reverse of %d is %s\n", num, rev);
	free(rev);
}
