#include <stdio.h>

void reverse(char **s)
{
    char *start = *s;
    char *end = start;
    while (*end)
        end++;

    end--;
    while (start < end) {
        char temp = *start;
        *start = *end;
        *end = temp;
        start++;
        end--;
    }
}

int main(int argc, char *argv[])
{
    char *s = argv[1];
    printf("Reverse of %s: ", s);
    reverse(&s);
    printf("%s\n", s);
    return 0;
}
