/* Count number of bits set in a 32 bit number without a loop */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int num = atoi(argv[1]);
    int set_bits = ((num>>7)&1) + ((num>>6)&1) + ((num>>5)&1) + ((num>>4)&1)
                 + ((num>>3)&1) + ((num>>2)&1) + ((num>>1)&1) + (num&1);
    printf("Number of set bits: %d\n", set_bits);
    return 0;
}
