#include <stdio.h>

int max(int a, int b) {
  return (a>b)?a:b;
}

int knapsack(int c, int w[], int v[], int n) {
  int A[n+1][c+1], i, j;

  for(i=0; i<=n; i++) {
    for(j=0; j<=c; j++) {
      if (i==0 || j==0) {
        A[i][j] = 0;
      }
      else if(w[i-1] <= j)
        A[i][j] = max(v[i-1]+A[i-1][j-w[i-1]], A[i-1][j]);
      else
        A[i][j] = A[i-1][j];
    }
  }

  for(i=0; i<=n; i++) {
    printf("\n");
    for(j=0; j<=c; j++) 
      printf("%d ", A[i][j]);
  }

  return A[n][c];
}

int main() {
  int w[] = {3, 8, 5};
  int v[] = {4, 6, 5};
  int c = 118;
  printf("\nMaximum value is: %d\n", knapsack(c, w, v, 3));
  return 0;
}
