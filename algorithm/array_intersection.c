/* Find intersection of two sorted arrays */

#include <stdio.h>

int main() {
  int a[] = { 1, 1, 2, 2, 2, 2, 3, 7 };
  int b[] = { 2, 3, 3, 3, 4, 7 };
  int c[10];
  int l1 = 8, l2 = 6;
  int elem = -1;
  int i = 0, j = 0, k = 0;
  
  while ( i < l1 && j < l2 ) {
    if ( a[i] == b[j] && a[i] != elem ) {
      elem = a[i];
      c[k++] = elem;
      i++;
      j++;
    }
    else if( a[i] < b[j] )
      i++;
    else
      j++;
  }

  for ( i = 0; i < k; i++ )
    printf("%d ", c[i]);
  printf("\n");
  return 0;
}
