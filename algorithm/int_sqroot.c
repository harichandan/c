#include <stdio.h>
#include <stdlib.h>

int sq(int n)
{
    return n * n;
}

int sqroot(int n)
{
    int low = 0, high = n, mid;
    while (low <= high) {
        mid = (low + high)/2;

        if (sq(mid) <= n && sq(mid + 1) > n)
            return mid;

        if (sq(mid) < n)
            low = mid + 1;
        else
            high = mid - 1;
    }
    return -1;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Syntax: <exe> <number>");
        exit(-1);
    }
    int num = atoi(argv[1]);
    printf("Integer square root of %d is %d\n", num, sqroot(num));
}
