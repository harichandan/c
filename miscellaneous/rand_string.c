#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

char *rand_string(int length) {
  const char char_set[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  int i=0, set_len = sizeof(char_set)-1;
  char *str = malloc(length);
  srand(time(NULL));
  
  for (i=0; i<length; i++)
    *(str+i) = char_set[rand() % set_len];
  *(str+i) = '\0';
  return str;
}

int isnum(char* str) {
  while (*str) {
    if (!isdigit(*str))
      return 0;
    else
      str++;
  }
  return 1;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Syntax: ./executable <length of random string>\n");
    exit(1);
  }

  if (!isnum(argv[1])) {
    printf("Length of string has to be a number\n");
    exit(1);
  }

  char* str = rand_string(atoi(argv[1]));

  printf("The random string is: %s\n", str);

  free(str);
  return 0;
}
