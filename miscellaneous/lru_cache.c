#include <stdio.h>
#include <stdlib.h>

// DLL Node
typedef struct node {
	int data;
	struct node *prev, *next;
} node;


// DLL
typedef struct dll {
	int count;
	int max_pages;
	struct node *head, *tail;
} dll;

// Hash to keep track of node pointers
typedef struct hash {
	int count;
	node* *array;
} hash;

dll *create_dll(int count) {
	dll *d = malloc(sizeof(dll));
	d->count = 0;
	d->head = d->tail = NULL;
	d->max_pages = 0;
	return d;
}

hash *create_hash(int count) {
	hash *h = malloc(sizeof(hash));
	h->count = count;
	h->array = malloc(count * sizeof(node*));
	for(int i=0; i<count; i++)
		h->array[i] = NULL;
	return h;
}

node *create_node(int page_number) {
	node *new_node = malloc(sizeof(node));
	new_node->prev = NULL;
	new_node->next = NULL;
	new_node->data = page_number;
	return new_node;
}

void del(dll *d) {
	if (!d) {
		printf("Queue is empty!\n");
		return;
	}
	
	node *temp = d->tail;
	
	if (d->tail == d->head) {
		d->tail = d->head = NULL;
		free(d->tail);
		return;
	}
	d->tail = d->tail->prev;
	d->tail->next = NULL;
	free(temp);
	d->count--;
}

void add(dll *d, hash *h, int page_number) {
	node *new_node = create_node(page_number);
	if(!d->head && !d->tail) {
		d->head = d->tail = new_node;
		printf("Added %d to DLL\n", page_number);
		h->array[page_number] = new_node;
		return;
	}
	new_node->next = d->head;
	if(new_node->next)
		new_node->next->prev = new_node;
	d->head = new_node;
	d->count++;
	printf("Added %d to DLL\n", page_number);
	h->array[page_number] = new_node;
}

void req_page(dll *d, hash *h, int page_number) {
	node *n = h->array[page_number];
	
	if (n == NULL) {
		add(d, h, page_number);
	}

	else if(d->head != n) {
		printf("%d is already in the hash table\n", page_number);
		n->prev->next = n->next;
		if(n->next)
			n->next->prev = n->prev;
		d->head->prev = n;
		n->next = d->head;
		n->prev = NULL;
		d->head = n;
	}
}

int main() {
	dll *d = create_dll(5);
	hash *h = create_hash(10);
	req_page(d, h, 1);
	req_page(d, h, 2);
	req_page(d, h, 5);
	req_page(d, h, 1);
	printf("%d\n", d->head->data);
	printf("%d\n", d->head->next->data);
	printf("%d\n", d->head->next->next->data);
}
