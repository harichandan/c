#include <stdio.h>
#include <stdlib.h>

struct AdjListNode {
  int dest;
  struct AdjListNode *next;
};

struct AdjList {
  struct AdjListNode *head;
};

struct Graph {
  int V;
  struct AdjList *array;
};

struct AdjListNode *newNode(int dest) {
  struct AdjListNode *node = malloc(sizeof(struct AdjListNode));
  node->dest = dest;
  node->next = NULL;
  return node;
}

struct Graph *createGraph(int V) {
  struct Graph *graph = malloc(sizeof(struct Graph));
  graph->V = V;
  graph->array = malloc(V*sizeof(struct AdjList));

  for(int i=0; i<V; i++) {
    graph->array[i].head = NULL;
  }

  return graph;
}

void addEdge(struct Graph *graph, int source, int destination) {
  struct AdjListNode *node = malloc(sizeof(struct AdjListNode));
  node->next = graph->array[source].head;
  graph->array[source].head = node;
  node->dest = destination;

  struct AdjListNode *source_node = malloc(sizeof(struct AdjListNode));
  source_node->next = graph->array[destination].head;
  graph->array[destination].head = source_node;
  source_node->dest = source;
}

void deleteGraph(struct Graph *graph) {
  for(int i=0; i<graph->V; i++) {
    struct AdjListNode *temp, *node = graph->array[i].head;
    while(node) {
      temp = node;
      free(temp);
      node = node->next;
    }
  }

  free(graph->array);
  free(graph);
}

void printGraph(struct Graph *graph) {
  int i;
  for(i=0; i<graph->V; i++) {
    printf("Adjacency List of Node %d:\n", i);
    struct AdjListNode *temp = graph->array[i].head;
    while (temp) {
      printf("%d -> ", temp->dest);
      temp = temp->next;
    }
    printf("\n");
  }  
}

int main() {
  struct Graph *graph = createGraph(5);
  addEdge(graph, 0, 1);
  addEdge(graph, 0, 4);
  addEdge(graph, 1, 2);
  addEdge(graph, 1, 3);
  addEdge(graph, 1, 4);
  addEdge(graph, 2, 3);
  addEdge(graph, 3, 4);
  printGraph(graph);
  deleteGraph(graph);
  return 0;
}
