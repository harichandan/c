#include <stdio.h>

#define INF 100000
#define N 4

// Recursive Approach
int min_cost(int cost[][N], int start, int dest) {
  
  if ((start == dest) || (start+1 == dest))
    return cost[start][dest];
  
  int min = cost[start][dest];

  for (int i=start+1; i<dest; i++) {
    int c = min_cost(cost, start, i) + min_cost(cost, i, dest);

    if (c < min)
      min = c;
  }
  return min;
}

// Dynamic Programming Approach
int min_cost_dp(int cost[][N]) {
  int distance[N];
  
  for (int i=0; i<N; i++)
    distance[i] = INF;

  distance[0] = 0;

  for (int i=0; i<N; i++) {
    for (int j=i+1; j<N; j++) {
      if (distance[j] > distance[i] + cost[i][j])
        distance[j] = distance[i] + cost[i][j];
    }
  }

  return distance[N-1];
}

int main() {
  int cost[N][N] = { {0, 15, 80, 90},
                     {INF, 0, 40, 50},
                     {INF, INF, 0, 70},
                     {INF, INF, INF, 0}    
                    };
  printf("The minimum cost is: %d\n", min_cost(cost, 0, N-1));
  printf("The minimum cost is: %d\n", min_cost_dp(cost));
  return 0;
}
