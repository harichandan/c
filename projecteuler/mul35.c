#include <stdio.h>

int sum(int n) {
    return n * (n + 1) / 2;
}

int mul35(int n) {
    return 3 * sum((n - 1)/3) + 5 * sum((n - 1) / 5) - 15 * sum((n - 1) / 15);
}

int main() {
    int i, sum = 0;
    for (i=1; i<1000; i++) {
        if (i%3 == 0 || i%5 == 0)
            sum+=i;
    }
    printf("Sum: %d\n", sum);
    printf("Sum from function: %d\n", mul35(1000));
    return 0;
}
