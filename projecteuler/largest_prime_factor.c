#include <stdio.h>
#include <math.h>

long prime_factor(long n) {
    long sqrt_n = sqrt(n);
    long max_factor = -1;
    while (!(n % 2))
        n >>= 1;

    for (int i = 3; i <= sqrt_n; i += 2) {
        if (n % i == 0) {
            max_factor = i;
        }
    }

    return max_factor;
}

int main() {
    printf("Largest prime factor of 600851475143 is %ld\n", prime_factor(600851475143));
    return 0;
}
