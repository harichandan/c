/*
 * Problem #5
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */

#include <stdio.h>

int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a%b);
}

int main() {
    int prod = 1;
    for (int i = 2; i<=20; i++) {
        if (prod % i != 0) {
            int gcd_prod = gcd(prod, i);
            printf("prod: %d, gcd: %d, num: %d\n", prod, gcd_prod, i);
            if (gcd_prod == 1)
                prod *= i;
            else
                prod *= i / gcd_prod;
        }
    }

    printf("%d\n", prod);
    return 0;
}
