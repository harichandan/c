#include <stdio.h>

int sum(int n) {
    return n * (n + 1) / 2;
}

int square_sum(int n) {
    return n * (n + 1) * (2 * n + 1) / 6;
}

int main() {
    int sum_n = sum(100);
    int sq_sum_n = square_sum(100);
    printf("Sum Difference: %d\n", sum_n * sum_n - sq_sum_n);
    return 0;
}
