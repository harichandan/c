#include <stdio.h>

long sum_even_fib(long limit) {
  long ef0 = 0, ef1 = 2;
  long sum = ef0 + ef1;
  while (ef1 <= limit) {
    long ef2 = 4 * ef1 + ef0;
    if (ef2  > limit)
      break;
    ef0 = ef1;
    ef1 = ef2;
    sum += ef2;
  }
  return sum;
}

int main() {
  long sum = 0;
  int f=1, s=1;
  int t;
  while (t <= 4000000) {
    t = f+s;
    f=s;
    s=t;
    if (t%2==0)
      sum+=t;
	}
  printf("Sum: %ld\n", sum);
  printf("Sum using the function: %ld\n", sum_even_fib(4000000));
	return 0;
}
