#include <stdio.h>

int square(int n) {
    return n*n;
}

int main() {
    for (int i=1; i<1000; i++) {
        for (int j=1; j<1000; j++) {
            for (int k=1; k<1000; k++) {
                if (square(j) + square(k) == square(i) && (i+j+k==1000)) {
                    printf("%d %d %d\n", i, j, k);
                    printf("%d\n", i*j*k);
                    break;
                }
            }
        }
    }
    return 0;
}
