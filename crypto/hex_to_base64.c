#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int hex(char c) {
  char hexstring[] = "0123456789abcdef";
  for (int i=0; i<16; i++) {
    if(c == hexstring[i])
      return i;
  }
  return -1;
}
int power(int base, int exponent) {
  int num = base;
  if (exponent == 0)
    return 1;
  else if (exponent == 1)
    return base;
  while (exponent > 1) {
    base = base * num;
    exponent--;
  }
  return base;
}

char* hex_to_base64(char *hexa) {
  char *h[16] = { "0000", "0001", "0010", "0011",
                  "0100", "0101", "0110", "0111",
                  "1000", "1001", "1010", "1011",
                  "1100", "1101", "1110", "1111" };

  char *b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  char temp[13];
  int i=0, l, k=0, j=0, temp_base = 0;
  for(l=0; *(hexa+l) != '\0'; l++);
  char *base64 = malloc(sizeof(char)*l);
  
  while(i<l) {
    strncpy(temp, h[hex(hexa[i])], 4);
    temp[4] = '\0';
    strncat(temp, h[hex(hexa[i+1])], 4);
    temp[8] = '\0';
    strncat(temp, h[hex(hexa[i+2])], 4);
    temp[12] = '\0';
    temp_base = 0;
    for(j=0; j<6; j++) {
      temp_base += (temp[j] - '0')*power(2, 5-j);
    }
    base64[k++] = *(b64+temp_base);
    temp_base = 0;
    for(j=6; j<12; j++) {
      temp_base += (temp[j] - '0')*power(2, 5-(j%6));
    }
    base64[k++] = *(b64+temp_base);
    i+=3;
  }
  base64[k] = '\0';
  return base64;
}

int main(int argc, char *argv[]) {
  char *string = argv[1];
  char *new_string;
  int l, ret_len;
  int count=0;
  for(l=0; *(string+l)!='\0'; l++);
  if(l%2) {
    printf("String length is odd. Exiting\n");
    return -1;
  }
  if(l%6 == 2) {
    new_string = malloc(sizeof(char)*(l+3));
    strncpy(new_string, string, l);
    *(new_string+(l++))='0';
    *(new_string+(l++))='0';
    *(new_string+(l++))='0';
    *(new_string+(l++))='0';
    count=2;
    *(new_string+l)='\0';
  }
  else if(l%6 == 4) {
    new_string = malloc(sizeof(char)*(l+5));
    strncpy(new_string, string, l);
    *(new_string+(l++))='0';
    *(new_string+(l++))='0';
    count=1;
    *(new_string+l)='\0';
  }
  else {
    new_string = malloc(sizeof(char)*(l+1));
    strncpy(new_string, string, l+1);
  }
  printf("%s\n", new_string);
  printf("Length = %d\n", l);
  char *result = hex_to_base64(new_string);
  for(l=0; *(result+l)!='\0'; l++);
  for(int i=0; i<count; i++) {
    if(*(result+l-1-i) == 'A')
      *(result+l-1-i) = '=';
  }
  printf("%s\n", result);
  free(result);
  free(new_string);
  return 0;
}
